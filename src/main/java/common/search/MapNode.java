package common.search;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;

public class MapNode {
	
	// Index information
	public final int value, begin, end;
	public final HashMap<Integer, MapNode> childs;
	
	// Used for debug purposes only
	public final ArrayList<Integer> parentValues;
	
	@SuppressWarnings("unchecked")
	public MapNode(ArrayList<Integer> parentValues, int value, int begin, int end) {
		this.parentValues = (ArrayList<Integer>) parentValues.clone();
		this.childs = new HashMap<>();
		this.value = value;
		this.begin = begin;
		this.end = end;
	}
	
	@Override
	public String toString() {
		return parentValues.toString() + "(" + begin + "," + end + ")";
	}

	public void add(MapNode node) {
		if (Objects.nonNull(node)) {
			childs.put(node.value, node);
		}
	}

}
