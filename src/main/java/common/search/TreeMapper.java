package common.search;

import java.util.ArrayList;
import java.util.Objects;

import common.Hypercube;

public class TreeMapper extends AbstractMapper {
	
	private MapNode root;
	
	// Minimum number of rows to allow sub-mapping 
	private final int minSplit;
	
	// Maximum number of dimensions to map
	protected int numMappedDimensions;
	
	public TreeMapper() {
		this(Integer.MAX_VALUE, 0);
	}
	
	public TreeMapper(int numMappedDimensions) {
		this(numMappedDimensions, 0);
	}
	
	public TreeMapper(int numMappedDimensions, int minSplit) {

		this.numMappedDimensions = numMappedDimensions;
		
		// Set the minimum number of rows to allow sub-mapping
		// When the value is 0 this parameter has no effect
		this.minSplit = minSplit;
	}

	@Override
	public Mapper buildMap(Hypercube[] H) {
		this.H = H;
		this.Wmax = 0;
		
		int numTotalDimensions = (H.length > 0) ? H[0].getNumDimensions() : 0;
		
		if (numMappedDimensions <= 1) {
			numMappedDimensions = 1;
		} else if (numMappedDimensions >= numTotalDimensions) {
			numMappedDimensions = numTotalDimensions;
		}

		
		// The root node maps the whole dataset
		this.root = new MapNode(new ArrayList<>(), -1, 0, H.length - 1);
		
		// Start recursive mapping from the first dimension
		doMaping(this.root, 0);
		
		return this;
	}
	
	private void doMaping(MapNode parent, int col) {

		// Stop mapping when all dimensions are mapped
		if (col == this.numMappedDimensions)
			return;
		
		// Stop sub-mapping when the parent node map less than minSplit hypercubes
		if (parent.end - parent.begin < this.minSplit)
			return;
		
		// Get the first value from the given range (minRowIdx, maxRowIdx)
		int value = this.H[parent.begin].getCoordAt(col);
		
		// debug only
		@SuppressWarnings("unchecked")
		ArrayList<Integer> parentValues = (ArrayList<Integer>) parent.parentValues.clone();
		parentValues.add(col, value);

		// Initialise the next range
		int begin = parent.begin;
		int end = -1;

		// map the values in the current range
		int i = parent.begin;
		for (; i <= parent.end; i++) {
		
			// when the value change the node is created
			if (H[i].getCoordAt(col) != value) {
				
				// mark the end of the current value 
				end = i - 1;
				
				// create node for 'value' in 'col'
				MapNode child = new MapNode(parentValues, value, begin, end);				
				parent.add(child);
				
				// map child values in the next dimension
				doMaping(child, col + 1);

				// start new range
				begin = i;

				// update value
				value = this.H[i].getCoordAt(col);
				
				// debug only
				parentValues.set(col, value);
			}
		}
		
		// debug only
		parentValues.set(col, value);

		// map last value
		end = i - 1;
		MapNode child = new MapNode(parentValues, value, begin, end);
		parent.add(child);

		doMaping(child, col + 1);
	}
	
	private int density(Hypercube hi, MapNode parent, int col) {
		int density = 0;
		
		if (parent.childs.isEmpty()) {
			for (int k = parent.begin; k <= parent.end; k++) {
				if (!isProspective(hi, H[k], col))
					break;
				if (isImmediate(hi, H[k])) {
					density += H[k].getDensity();
				}
			}
		} else {

			int lftVal = hi.getCoordAt(col) - 1;
			int midVal = hi.getCoordAt(col);
			int rgtVal = hi.getCoordAt(col) + 1;
			
			MapNode lftNode = parent.childs.get(lftVal);
			MapNode midNode = parent.childs.get(midVal);
			MapNode rgtNode = parent.childs.get(rgtVal);
			
			int nextCol = Math.min(col + 1, hi.getNumDimensions() - 1);
			
			if (Objects.nonNull(lftNode)) {
				density += density(hi, lftNode, nextCol);
			}
			
			if (Objects.nonNull(midNode)) {
				density += density(hi, midNode, nextCol);
			}
			
			if (Objects.nonNull(rgtNode)) {
				density += density(hi, rgtNode, nextCol);
			}

		}
		
		return density;
	}

	@Override
	public int[] getNeighborhoodDensities() {
		int n = H.length;
		int[] W = new int[n];

		for (int i = 0; i < n; i++) {
			W[i] = density(H[i], root, 0);
			Wmax = Math.max(Wmax, W[i]);
		}

		return W;
	}
	
}
